module.exports = {
  smsApi: {
    frequency: "*/1 * * * *",
    handler: `cronjobs/sayHello`,
  },

  responseApi: {
    frequency: "*/5 * * * * *",
    handler: `cronjobs/sayBye`,
  },
};
