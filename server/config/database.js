const sql = require("mssql");

const config = {
  user: process.env.USER,
  password: process.env.PASSWORD,
  server: process.env.SERVER,
  database: process.env.DATABASE,
  driver: process.env.DRIVER,
  options: {
    encrypt: false,
    trustedConnection: true,
    trustServerCertificate: true,
  },
};

const dbConnection = async () => {
  const pool = await new sql.connect(config);
  return pool;
};

const dbPool = new sql.ConnectionPool(config).connect();

module.exports = {
  dbConnection,
  sql,
  dbPool,
};
