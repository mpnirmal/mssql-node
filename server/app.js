const express = require("express");
const app = express();
const cookieParser = require("cookie-parser");
const cors = require("cors");

const errorMiddleware = require("./middleware/error");
const ErrorHandler = require("./utils/errorHandler");
const dotenv = require("dotenv");

// config
dotenv.config({ path: "config/config.env" });

// import routes
const user = require("./routes/userRoute");

app.use(cors());
app.use(express.json());
app.use(cookieParser());
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) =>
  res.send(`server is working fine
<a href="/api/v1/users">getAllUsers</a>`)
);

app.use("/api/v1", user);

// Page Not Found
app.all("*", (req, res, next) => {
  next(new ErrorHandler(`${req.originalUrl} Resource Not Found`, 404));
});

// Error handling middleware
app.use(errorMiddleware);

module.exports = app;
