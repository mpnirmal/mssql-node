const cron = require("node-cron");
const { resolve } = require("path");

module.exports = {
  initCron: (cronjob) => {
    Object.keys(cronjob).forEach((key) => {
      if (cron.validate(cronjob[key].frequency)) {
        cron.schedule(cronjob[key].frequency, () => {
          const handler = require(resolve(cronjob[key].handler));
          handler();
        });
      }
    });
  },
};
