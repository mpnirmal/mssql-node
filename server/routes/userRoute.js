const express = require("express");
const {
  createNewUser,
  getUsers,
  getSingleUser,
  deleteUser,
  createNewSurvey,
} = require("../controllers/userController");

const router = express.Router();

router.route("/user/new").post(createNewUser);
router.route("/survey/new").post(createNewSurvey);

router.route("/users").get(getUsers);

router.route("/user/:id").get(getSingleUser).delete(deleteUser).put();

module.exports = router;
