const { dbConnection } = require("../config/database");
const createAsyncError = require("../middleware/createAsyncError");

module.exports = createAsyncError(async () => {
  const db = await dbConnection();

  const request = await db
    .request()
    .query(`SELECT * FROM survey_reports WHERE sms_flag = 0`);

  if (request.recordset) {
    request.recordset.forEach(async (val) => {
      if (val.sms_flag === false) {
        console.log(val.id);
      }
    });
  }
});
