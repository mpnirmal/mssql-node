const app = require("./app");
const { dbConnection, dbPool } = require("./config/database");
const cronJob = require("./utils/cronJobs");
const scheduler = require("./cronScheduler/scheduler");

// Handling Uncaught Exception Error
process.on("uncaughtException", (err) => {
  console.log(`Error: ${err.message}`);
  console.log("Shutting down the server due to Uncaught Exception");

  process.exit(1);
});

// database connection
dbConnection().then((pool) =>
  console.log("Connected to SQL Server", pool.config.database)
);

// dbPool.then((pool) =>
//   console.log("Connected to SQL Server", pool.config.database)
// );

// Schedule CronJobs
scheduler.initCron(cronJob);

const server = app.listen(process.env.PORT, () => {
  console.log(`server is working on http://localhost:${process.env.PORT}`);
});

// Unhandled Promise Rejection Error
process.on("unhandledRejection", (err) => {
  console.log(`Error: ${err.message}`);
  console.log("Shutting down the server due to Unhandled Promise Rejection");

  server.close(() => process.exit(1));
});
