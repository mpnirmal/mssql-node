const { sql, dbConnection, dbPool } = require("../config/database");
const createAsyncError = require("../middleware/createAsyncError");
const {
  createTableQuery,
  createDataIntoUserTable,
  getAllUsers,
  createSurveyTable,
  createDataIntoSurveyTable,
} = require("../models/userModel");
const ErrorHandler = require("../utils/errorHandler");

exports.getUsers = createAsyncError(async (req, res, next) => {
  // const db = await dbConnection();
  const db = await dbPool;

  console.log(db);

  const users = await db.request().query(getAllUsers || "userPhone");

  res.status(200).json({ success: true, users: users.recordset });
});

exports.createNewUser = createAsyncError(async (req, res, next) => {
  const { id, name = name ?? "vicky", age, email, is_active } = req.body;

  const db = await dbConnection();

  const request = db.request();

  await request.query(createTableQuery);

  request.input("id", sql.Int, id);
  request.input("name", sql.VarChar(50), name);
  request.input("age", sql.Int, age);
  request.input("email", sql.VarChar(100), email);
  request.input("is_active", sql.Bit, is_active);

  await request.query(createDataIntoUserTable);

  res.status(200).json({ success: true });
});

exports.getSingleUser = createAsyncError(async (req, res, next) => {
  const db = await dbConnection();

  const user = await db
    .request()
    .input("id", req.params.id)
    .query("SELECT * FROM user_info WHERE id = @id");

  res.status(200).json({ success: true, user: user.recordset[0] });
});

exports.deleteUser = createAsyncError(async (req, res, next) => {
  const db = await dbConnection();

  const user = await db
    .request()
    .input("id", req.params.id)
    .query("DELETE FROM user_info WHERE id = @id");

  if (user.rowsAffected[0] === 0) {
    return next(new ErrorHandler("User Not Found", 404));
  }

  res.status(200).json({ success: true, message: "User deleted successfully" });
});

exports.getUsersCount = createAsyncError(async (req, res, next) => {
  const db = await dbConnection();
  const count = await db.request().query("SELECT COUNT(*) FROM user_info");
  res.status(200).json({ success: true, count: count.recordset[0][""] });
});

exports.createNewSurvey = createAsyncError(async (req, res, next) => {
  const { id, phone } = req.body;

  const db = await dbConnection();

  const request = db.request();

  await request.query(createSurveyTable);

  request.input("id", sql.Int, id);
  request.input("phone", sql.Int, phone);

  await request.query(createDataIntoSurveyTable);

  res.status(200).json({ success: true });
});
