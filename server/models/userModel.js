const createTableQuery = `
  IF NOT EXISTS (
    SELECT * FROM INFORMATION_SCHEMA.TABLES 
    WHERE TABLE_NAME = 'user_info'
  )
  BEGIN
    CREATE TABLE user_info (
      id INT PRIMARY KEY,
      name VARCHAR(50) NOT NULL,
      age INT,
      email VARCHAR(100) NOT NULL,
      is_active BIT NOT NULL
    )
  END
`;

const createSurveyTable = `
  IF NOT EXISTS (
    SELECT * FROM INFORMATION_SCHEMA.TABLES 
    WHERE TABLE_NAME = 'survey_reports'
  )
  BEGIN
    CREATE TABLE survey_reports (
      id INTEGER PRIMARY KEY,
      phone INT UNIQUE,
      sms_flag BIT DEFAULT 0
    )
  END
`;

const createDataIntoUserTable = `
INSERT INTO user_info (id, name, age, email, is_active) 
VALUES (@id, @name, @age, @email, @is_active)
`;

const createDataIntoSurveyTable = `
INSERT INTO survey_reports (id, phone) 
VALUES (@id, @phone)
`;

const getAllUsers = `SELECT * FROM user_info`;

const selectSurveyIfSmsFlagZero = `SELECT * FROM survey_reports WHERE sms_flag = 0`;

module.exports = {
  createTableQuery,
  createSurveyTable,
  createDataIntoUserTable,
  createDataIntoSurveyTable,
  getAllUsers,
  selectSurveyIfSmsFlagZero,
};
